package com.dy.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class CheckInServiceTest {
    @Autowired
    private ICheckInService checkInService;


    @Test
    void testGetAll() {
        checkInService.list();
    }

    @Test
    void testGetPage() {
        IPage page = new Page(1, 2);
        checkInService.page(page);
        System.out.println(page.getCurrent());
        System.out.println(page.getSize());
        System.out.println(page.getTotal());
        System.out.println(page.getPages());
        System.out.println(page.getRecords());
    }

        /*@Test
        void testGetBy(){
            QueryWrapper<AwardAndHonorController> qw = new QueryWrapper<>();
            qw.like("full_name","王");
            awardAndHonorService.list(qw);
        }*/
}
