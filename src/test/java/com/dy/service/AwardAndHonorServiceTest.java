package com.dy.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.dy.domain.AwardAndHonor;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
@SpringBootTest
public class AwardAndHonorServiceTest {
        @Autowired
        private IAwardAndHonorService awardAndHonorService;

        @Test
        void testGetById() {
            System.out.println(awardAndHonorService.getById(1));
        }



        @Test
        void testUpdate() {
            AwardAndHonor awardAndHonor = new AwardAndHonor();
            awardAndHonor.setId(1L);
            awardAndHonorService.updateById(awardAndHonor);
        }

        @Test
        void testDelete() {
            awardAndHonorService.removeById(1L);
        }

        @Test
        void testGetAll() {
            awardAndHonorService.list();
        }

        @Test
        void testGetPage() {
            IPage page = new Page(1, 2);
            awardAndHonorService.page(page);
            System.out.println(page.getCurrent());
            System.out.println(page.getSize());
            System.out.println(page.getTotal());
            System.out.println(page.getPages());
            System.out.println(page.getRecords());
        }

        /*@Test
        void testGetBy(){
            QueryWrapper<AwardAndHonorController> qw = new QueryWrapper<>();
            qw.like("full_name","王");
            awardAndHonorService.list(qw);
        }*/
    }
