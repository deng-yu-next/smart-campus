package com.dy.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.dy.domain.Student;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class StudentServiceTest {
    @Autowired
    private IStudentService studentService;

    @Test
    void testGetById() {
        System.out.println(studentService.getById(1));
    }

    @Test
    void testSave() {
        Student student = new Student();
        student.setStudentNumber("E02025222");
        student.setPassword("123456");
        student.setFullName("Jim");
        student.setGender("男");
        studentService.save(student);
    }

    @Test
    void testUpdate() {
        Student student = new Student();
        student.setId(1L);
        student.setStudentNumber("E01916332");
        student.setPassword("123456");
        student.setFullName("王小明");
        student.setGender("男");
        studentService.updateById(student);
    }

    @Test
    void testDelete() {
        studentService.removeById(1L);
    }

    @Test
    void testGetAll() {
        studentService.list();
    }

    @Test
    void testGetPage() {
        IPage page = new Page(1, 2);
        studentService.page(page);
        System.out.println(page.getCurrent());
        System.out.println(page.getSize());
        System.out.println(page.getTotal());
        System.out.println(page.getPages());
        System.out.println(page.getRecords());
    }

    @Test
    void testGetBy(){
        QueryWrapper<Student> qw = new QueryWrapper<>();
        qw.like("full_name","王");
        studentService.list(qw);
    }
}
