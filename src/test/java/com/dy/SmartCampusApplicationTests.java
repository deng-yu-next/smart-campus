package com.dy;

import com.dy.dao.StudentDao;
import com.dy.domain.Student;
import com.dy.domain.StudentStatus;
import com.dy.service.IStudentStatusService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
class SmartCampusApplicationTests {
    @Autowired
    private StudentDao studentDao;
    private IStudentStatusService sss;

    @Test
    void contextLoads() {
    /*
    *   TODO：按条件查询
    *   //方式一：按条件查询
        QueryWrapper qw = new QueryWrapper();
        qw.lt("age",18);
        List<User> userList = userDao.selectList(qw);
        System.out.println(userList);
        *
        //方式二：lambda格式按条件查询
        QueryWrapper<User> qw = new QueryWrapper<User>();
        qw.lambda().lt(User::getAge, 10);
        List<User> userList = userDao.selectList(qw);
        System.out.println(userList);

        //方式三：lambda格式按条件查询
        LambdaQueryWrapper<User> lqw = new LambdaQueryWrapper<User>();
        lqw.lt(User::getAge, 10);
        List<User> userList = userDao.selectList(lqw);
        System.out.println(userList);
        //推荐使用方式三，其中lt是less than 小于的意思，其他mp使用自己查询
        *
        //并且与或者关系
        LambdaQueryWrapper<User> lqw = new LambdaQueryWrapper<User>();
        //并且关系：10到30岁之间
        //lqw.lt(User::getAge, 30).gt(User::getAge, 10);
        //或者关系：小于10岁或者大于30岁
        lqw.lt(User::getAge, 10).or().gt(User::getAge, 30);
        List<User> userList = userDao.selectList(lqw);
        System.out.println(userList);
        */

        /*
        TODO：IPage对象封装了分页操作相关的数据
        IPage page  = new Page(2,3);
        userDao.selectPage(page,null);
        System.out.println("当前页码值："+page.getCurrent());
        System.out.println("每页显示数："+page.getSize());
        System.out.println("一共多少页："+page.getPages());
        System.out.println("一共多少条数据："+page.getTotal());
        System.out.println("数据："+page.getRecords());
        */


        /*
        TODO：查询投影
        //如果只想查数据库的部分字段，可以使用查询投影
        //第一种方法
        LambdaQueryWrapper<User> lqw = new LambdaQueryWrapper<User>();
        lqw.select(User::getId,User::getName,User::getAge);
        //第二种方法
        QueryWrapper<User> lqw = new QueryWrapper<User>();
        lqw.select("id","name","age","tel");

        List<User> userList = userDao.selectList(lqw);
        System.out.println(userList);

        //查询个数和分组
        QueryWrapper<User> lqw = new QueryWrapper<User>();
        lqw.select("count(*) as count, tel");
        lqw.groupBy("tel");
        List<Map<String, Object>> userList = userDao.selectMaps(lqw);
        System.out.println(userList);

        * */

        /*
        TODO：条件查询
        LambdaQueryWrapper<User> lqw = new LambdaQueryWrapper<User>();
        //等同于=
        lqw.eq(User::getName,"Jerry").eq(User::getPassword,"jerry");
        User loginUser = userDao.selectOne(lqw);
        System.out.println(loginUser);

        LambdaQueryWrapper<User> lqw = new LambdaQueryWrapper<User>();
        //范围查询 lt le gt ge eq between
        lqw.between(User::getAge,10,30);
        List<User> userList = userDao.selectList(lqw);
        System.out.println(userList);

        LambdaQueryWrapper<User> lqw = new LambdaQueryWrapper<User>();
        //模糊匹配 like
        lqw.likeLeft(User::getName,"J");
        List<User> userList = userDao.selectList(lqw);
        System.out.println(userList);
        */

        /*
        TODO：删除指定多条数据
        List<Long> list = new ArrayList<>();
        list.add(1402551342481838081L);
        list.add(1402553134049501186L);
        list.add(1402553619611430913L);
        userDao.deleteBatchIds(list);
        查询指定多条数据
        List<Long> list = new ArrayList<>();
        list.add(1L);
        list.add(3L);
        list.add(4L);
        userDao.selectBatchIds(list);
        */
    }

    @Test
    void TestDemo() {
        Student student = studentDao.selectById(1);
        System.out.println(student);
    }

}
