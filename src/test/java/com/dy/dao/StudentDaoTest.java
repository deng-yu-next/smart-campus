package com.dy.dao;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.dy.domain.Student;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class StudentDaoTest {
    @Autowired
    private StudentDao studentDao;

    @Test
    void testGetById(){
        System.out.println(studentDao.selectById(1));
    }

    @Test
    void testSave(){
        Student student = new Student();
        student.setStudentNumber("E02022222");
        student.setPassword("123456");
        student.setFullName("Tom");
        student.setGender("男");
        studentDao.insert(student);
    }

    @Test
    void testUpdate(){
        Student student = new Student();
        student.setId(1L);
        student.setStudentNumber("E01916332");
        student.setPassword("123456");
        student.setFullName("王小明");
        student.setGender("男");
        studentDao.updateById(student);
    }

    @Test
    void testDelete(){
        studentDao.deleteById(1713196574126919682L);
    }

    @Test
    void testGetAll(){
        studentDao.selectList(null);
    }

    @Test
    void testGetPage(){
        IPage page = new Page(1,2);
        studentDao.selectPage(page, null);
        System.out.println(page.getCurrent());
        System.out.println(page.getSize());
        System.out.println(page.getTotal());
        System.out.println(page.getPages());
        System.out.println(page.getRecords());
    }

    @Test
    void testGetBy(){
        QueryWrapper<Student> qw = new QueryWrapper<>();
        qw.like("full_name","王");
        studentDao.selectList(qw);
    }
}
