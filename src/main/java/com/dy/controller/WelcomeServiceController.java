package com.dy.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.dy.domain.WelcomeService;
import com.dy.service.IWelcomeServiceService;
import com.dy.utils.Code;
import com.dy.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author dengyu
 * @since 2023-10-13
 */
@RestController
@RequestMapping("/welcome")
public class WelcomeServiceController {
    @Autowired
    private IWelcomeServiceService welcomeServiceService;

    /**
     * 查询所有迎新服务数据
     *
     * @return Result型封装的json
     * */
    @GetMapping
    public Result getALL(){
        List<WelcomeService> welcomeServices = welcomeServiceService.list();
        Integer code = welcomeServices != null ? Code.GET_OK : Code.GET_ERR;
        String msg = welcomeServices != null ? "数据库查询成功" : "数据查询失败，请重试！";
        return new Result(code, welcomeServices, msg);
    }

    /**
     * 添加迎新服务数据
     *
     * @param welcomeService 提交的迎新服务数据
     * @return Result型封装的json
     * */
    @PostMapping
    public Result sava(@RequestBody WelcomeService welcomeService){
        boolean flag = welcomeServiceService.save(welcomeService);
        Integer code = flag ? Code.SAVE_OK : Code.SAVE_ERR;
        String msg = flag ? "数据添加成功" : "数据添加失败，请重试！";
        return new Result(code, flag, msg);
    }

    /**
     * 更新迎新服务数据
     *
     * @param welcomeService 提交的迎新服务更新数据
     * @return Result型封装的json
     * */
    @PutMapping
    public Result update(@RequestBody WelcomeService welcomeService){
        boolean flag = welcomeServiceService.updateById(welcomeService);
        Integer code = flag ? Code.UPDATE_OK : Code.UPDATE_ERR;
        String msg = flag ? "数据更新成功" : "数据更新失败，请重试！";
        return new Result(code, flag, msg);
    }

    /**
     * 根据id删除迎新服务数据
     *
     * @param id 删除id
     * @return Result型封装的json
     * */
    @DeleteMapping("{id}")
    public Result delete(@PathVariable Integer id) {
        boolean flag = welcomeServiceService.removeById(id);
        Integer code = flag ? Code.DELETE_OK : Code.DELETE_ERR;
        String msg = flag ? "数据删除成功" : "数据删除失败，请重试！";
        return new Result(code, flag, msg);
    }

    /**
     * 根据id查询迎新服务的请求
     *
     * @param id 查询id
     * @return Result型封装的json
     */
    @GetMapping("{id}")
    public Result getById(@PathVariable Integer id){
        WelcomeService welcomeService = welcomeServiceService.getById(id);
        Integer code = welcomeService != null ? Code.GET_OK : Code.GET_ERR;
        String msg = welcomeService != null ? "数据查询成功" : "数据查询失败，请重试！";
        return new Result(code, welcomeService, msg);
    }

    /**
     * 分页查询，提供基于条件的分页查询
     *
     * @param currentPage 当前页
     * @param pageSize    分页大小
     * @param welcomeService     查询条件，默认为空
     * @return Result型封装的json
     */
    @GetMapping("{currentPage}/{pageSize}")
    public Result getPage(@PathVariable int currentPage, @PathVariable int pageSize, WelcomeService welcomeService){
        IPage<WelcomeService> page = welcomeServiceService.getPage(currentPage, pageSize, welcomeService);
        if(currentPage > page.getPages()){
            page = welcomeServiceService.getPage(currentPage, pageSize, welcomeService);
        }
        Integer code = page != null ? Code.GET_OK : Code.GET_ERR;
        String msg = page != null ? "分页查询成功" : "分页查询失败，请重试！";
        return new Result(code, page, msg);
    }
}

