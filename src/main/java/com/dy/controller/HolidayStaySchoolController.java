package com.dy.controller;


import com.dy.domain.HolidayStaySchool;
import com.dy.service.IHolidayStaySchoolService;
import java.util.List;
import com.dy.utils.Code;
import com.dy.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.baomidou.mybatisplus.core.metadata.IPage;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author dengyu
 * @since 2023-10-13
 */
@RestController
@RequestMapping("/holiday")
public class HolidayStaySchoolController {
    @Autowired
    private IHolidayStaySchoolService holidayStaySchoolService;

    /**
     * 查询所有假期留校数据
     *
     * @return Result型封装的json
     * */
    @GetMapping
    public Result getALL(){
        List<HolidayStaySchool> holidaystayschools = holidayStaySchoolService.list();
        Integer code = holidaystayschools != null ? Code.GET_OK : Code.GET_ERR;
        String msg = holidaystayschools != null ? "数据库查询成功" : "数据查询失败，请重试！";
        return new Result(code, holidaystayschools, msg);
    }

    /**
     * 添加新的假期留校数据
     *
     * @param holidayStaySchool 提交的假期留校数据
     * @return Result型封装的json
     * */
    @PostMapping
    public Result sava(@RequestBody HolidayStaySchool holidayStaySchool){
        boolean flag = holidayStaySchoolService.save(holidayStaySchool);
        Integer code = flag ? Code.SAVE_OK : Code.SAVE_ERR;
        String msg = flag ? "数据添加成功" : "数据添加失败，请重试！";
        return new Result(code, flag, msg);
    }

    /**
     * 更新假期留校数据
     *
     * @param holidayStaySchool 提交的假期留校更新数据
     * @return Result型封装的json
     * */
    @PutMapping
    public Result update(@RequestBody HolidayStaySchool holidayStaySchool){
        boolean flag = holidayStaySchoolService.updateById(holidayStaySchool);
        Integer code = flag ? Code.UPDATE_OK : Code.UPDATE_ERR;
        String msg = flag ? "数据更新成功" : "数据更新失败，请重试！";
        return new Result(code, flag, msg);
    }

    /**
     * 根据id删除假期留校数据
     *
     * @param id 删除id
     * @return Result型封装的json
     * */
    @DeleteMapping("{id}")
    public Result delete(@PathVariable Integer id) {
        boolean flag = holidayStaySchoolService.removeById(id);
        Integer code = flag ? Code.DELETE_OK : Code.DELETE_ERR;
        String msg = flag ? "数据删除成功" : "数据删除失败，请重试！";
        return new Result(code, flag, msg);
    }

    /**
     * 根据id查询信息的请求
     *
     * @param id 查询id
     * @return Result型封装的json
     */
    @GetMapping("{id}")
    public Result getById(@PathVariable Integer id){
        HolidayStaySchool holidayStaySchool = holidayStaySchoolService.getById(id);
        Integer code = holidayStaySchool != null ? Code.GET_OK : Code.GET_ERR;
        String msg = holidayStaySchool != null ? "数据查询成功" : "数据查询失败，请重试！";
        return new Result(code, holidayStaySchool, msg);
    }

    /**
     * 分页查询，提供基于条件的分页查询
     *
     * @param currentPage 当前页
     * @param pageSize    分页大小
     * @param holidayStaySchool     查询条件，默认为空
     * @return Result型封装的json
     */
    @GetMapping("{currentPage}/{pageSize}")
    public Result getPage(@PathVariable int currentPage, @PathVariable int pageSize, HolidayStaySchool holidayStaySchool){
        IPage<HolidayStaySchool> page = holidayStaySchoolService.getPage(currentPage, pageSize, holidayStaySchool);;
        if(currentPage > page.getPages()){
            page = holidayStaySchoolService.getPage(currentPage, pageSize, holidayStaySchool);
        }
        Integer code = page != null ? Code.GET_OK : Code.GET_ERR;
        String msg = page != null ? "分页查询成功" : "分页查询失败，请重试！";
        return new Result(code, page, msg);
    }

}

