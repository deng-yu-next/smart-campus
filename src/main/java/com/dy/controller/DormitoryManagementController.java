package com.dy.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.dy.domain.DormitoryManagement;
import com.dy.service.IDormitoryManagementService;
import com.dy.utils.Code;
import com.dy.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;
/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author dengyu
 * @since 2023-10-13
 */
@RestController
@RequestMapping("/dormitory/management")
public class DormitoryManagementController {

    @Autowired
    private IDormitoryManagementService dormitoryManagementService;

    /**
     * 查询所有宿舍管理数据
     *
     * @return Result型封装的json
     */
    @GetMapping
    public Result getAll() {
        List<DormitoryManagement> dormitoryManagements = dormitoryManagementService.list();
        Integer code = dormitoryManagements != null ? Code.GET_OK : Code.GET_ERR;
        String msg = dormitoryManagements != null ? "数据查询成功" : "数据查询失败，请重试！";
        return new Result(code, dormitoryManagements, msg);
    }

    /**
     * 添加新的宿舍管理的请求
     *
     * @param dormitoryManagement 提交的宿舍管理更新数据
     * @return Result型封装的json
     */
    @PostMapping
    public Result save(@RequestBody DormitoryManagement dormitoryManagement) {
        boolean flag = dormitoryManagementService.save(dormitoryManagement);
        Integer code = flag ? Code.SAVE_OK : Code.SAVE_ERR;
        String msg = flag ? "数据添加成功" : "数据添加失败，请重试！";
        return new Result(code, flag, msg);
    }

    /**
     * 更新宿舍管理的请求
     *
     * @param dormitoryManagement 提交的宿舍管理更新数据
     * @return Result型封装的json
     */
    @PutMapping
    public Result update(@RequestBody DormitoryManagement dormitoryManagement) {
        boolean flag = dormitoryManagementService.updateById(dormitoryManagement);
        Integer code = flag ? Code.UPDATE_OK : Code.UPDATE_ERR;
        String msg = flag ? "数据更新成功" : "数据更新失败，请重试！";
        return new Result(code, flag, msg);
    }

    /**
     * 根据id删除宿舍管理的请求
     *
     * @param id 删除id
     * @return Result型封装的json
     */
    @DeleteMapping("{id}")
    public Result delete(@PathVariable Integer id) {
        boolean flag = dormitoryManagementService.removeById(id);
        Integer code = flag ? Code.DELETE_OK : Code.DELETE_ERR;
        String msg = flag ? "数据删除成功" : "数据删除失败，请重试！";
        return new Result(code, flag, msg);
    }

    /**
     * 根据id查询宿舍管理信息的请求
     *
     * @param id 查询id
     * @return Result型封装的json
     */
    @GetMapping("{id}")
    public Result getById(@PathVariable Integer id) {
        DormitoryManagement dormitoryManagement = dormitoryManagementService.getById(id);
        Integer code = dormitoryManagement != null ? Code.GET_OK : Code.GET_ERR;
        String msg = dormitoryManagement != null ? "数据查询成功" : "数据查询失败，请重试！";
        return new Result(code, dormitoryManagement, msg);
    }

    /**
     * 分页查询，提供基于条件的分页查询
     *
     * @param currentPage 当前业
     * @param pageSize    分页大小
     * @param dormitoryManagement    查询条件，默认为空
     * @return Result型封装的json
     */
    @GetMapping("{currentPage}/{pageSize}")
    public Result getPage(@PathVariable int currentPage, @PathVariable int pageSize, DormitoryManagement dormitoryManagement) {


        IPage<DormitoryManagement> page = dormitoryManagementService.getPage(currentPage, pageSize, dormitoryManagement);
        //如果当前页码值大于了总页码值，那么重新执行查询操作，使用最大页码值作为当前页码值
        if (currentPage > page.getPages()) {
            page = dormitoryManagementService.getPage(currentPage, pageSize, dormitoryManagement);
        }
        Integer code = page != null ? Code.GET_OK : Code.GET_ERR;
        String msg = page != null ? "分页查询成功" : "分页查询失败，请重试！";
        return new Result(code, page, msg);
    }
}

