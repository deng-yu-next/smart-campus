package com.dy.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.dy.domain.FinancialAssistance;
import com.dy.service.IFinancialAssistanceService;
import com.dy.utils.Code;
import com.dy.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;
/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author dengyu
 * @since 2023-10-13
 */
@RestController
@RequestMapping("/assistance")
public class FinancialAssistanceController {

    @Autowired
    private IFinancialAssistanceService financialAssistanceService;

    /**
     * 查询所有资助发放数据
     *
     * @return Result型封装的json
     */
    @GetMapping
    public Result getAll() {
        List<FinancialAssistance> financialAssistances = financialAssistanceService.list();
        Integer code = financialAssistances != null ? Code.GET_OK : Code.GET_ERR;
        String msg = financialAssistances != null ? "数据查询成功" : "数据查询失败，请重试！";
        return new Result(code, financialAssistances, msg);
    }

    /**
     * 添加新的资助发放的请求
     *
     * @param financialAssistance 提交的资助发放更新数据
     * @return Result型封装的json
     */
    @PostMapping
    public Result save(@RequestBody FinancialAssistance financialAssistance) {
        boolean flag = financialAssistanceService.save(financialAssistance);
        Integer code = flag ? Code.SAVE_OK : Code.SAVE_ERR;
        String msg = flag ? "数据添加成功" : "数据添加失败，请重试！";
        return new Result(code, flag, msg);
    }

    /**
     * 更新资助发放的请求
     *
     * @param financialAssistance 提交的资助发放更新数据
     * @return Result型封装的json
     */
    @PutMapping
    public Result update(@RequestBody FinancialAssistance financialAssistance) {
        boolean flag = financialAssistanceService.updateById(financialAssistance);
        Integer code = flag ? Code.UPDATE_OK : Code.UPDATE_ERR;
        String msg = flag ? "数据更新成功" : "数据更新失败，请重试！";
        return new Result(code, flag, msg);
    }

    /**
     * 根据id删除资助发放的请求
     *
     * @param id 删除id
     * @return Result型封装的json
     */
    @DeleteMapping("{id}")
    public Result delete(@PathVariable Integer id) {
        boolean flag = financialAssistanceService.removeById(id);
        Integer code = flag ? Code.DELETE_OK : Code.DELETE_ERR;
        String msg = flag ? "数据删除成功" : "数据删除失败，请重试！";
        return new Result(code, flag, msg);
    }

    /**
     * 根据id查询资助发放信息的请求
     *
     * @param id 查询id
     * @return Result型封装的json
     */
    @GetMapping("{id}")
    public Result getById(@PathVariable Integer id) {
        FinancialAssistance financialAssistance = financialAssistanceService.getById(id);
        Integer code = financialAssistance != null ? Code.GET_OK : Code.GET_ERR;
        String msg = financialAssistance != null ? "数据查询成功" : "数据查询失败，请重试！";
        return new Result(code, financialAssistance, msg);
    }

    /**
     * 分页查询，提供基于条件的分页查询
     *
     * @param currentPage 当前业
     * @param pageSize    分页大小
     * @param financialAssistance    查询条件，默认为空
     * @return Result型封装的json
     */
    @GetMapping("{currentPage}/{pageSize}")
    public Result getPage(@PathVariable int currentPage, @PathVariable int pageSize, FinancialAssistance financialAssistance) {


        IPage<FinancialAssistance> page = financialAssistanceService.getPage(currentPage, pageSize, financialAssistance);
        //如果当前页码值大于了总页码值，那么重新执行查询操作，使用最大页码值作为当前页码值
        if (currentPage > page.getPages()) {
            page = financialAssistanceService.getPage(currentPage, pageSize, financialAssistance);
        }
        Integer code = page != null ? Code.GET_OK : Code.GET_ERR;
        String msg = page != null ? "分页查询成功" : "分页查询失败，请重试！";
        return new Result(code, page, msg);
    }
}

