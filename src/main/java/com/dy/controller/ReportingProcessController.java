package com.dy.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.dy.domain.ReportingProcess;
import com.dy.service.IReportingProcessService;
import com.dy.utils.Code;
import com.dy.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author dengyu
 * @since 2023-10-13
 */
@RestController
@RequestMapping("/reporting")
public class ReportingProcessController {
    @Autowired
    private IReportingProcessService reportingProcessService;

    /**
     * 查询所有报道流程数据
     *
     * @return Result型封装的json
     * */
    @GetMapping
    public Result getALL(){
        List<ReportingProcess> reportingProcesses = reportingProcessService.list();
        Integer code = reportingProcesses != null ? Code.GET_OK : Code.GET_ERR;
        String msg = reportingProcesses != null ? "数据库查询成功" : "数据查询失败，请重试！";
        return new Result(code, reportingProcesses, msg);
    }

    /**
     * 添加报道流程数据
     *
     * @param reportingProcess 提交的报道流程数据
     * @return Result型封装的json
     * */
    @PostMapping
    public Result sava(@RequestBody ReportingProcess reportingProcess){
        boolean flag = reportingProcessService.save(reportingProcess);
        Integer code = flag ? Code.SAVE_OK : Code.SAVE_ERR;
        String msg = flag ? "数据添加成功" : "数据添加失败，请重试！";
        return new Result(code, flag, msg);
    }

    /**
     * 更新报道流程数据
     *
     * @param reportingProcess 提交的报道流程更新数据
     * @return Result型封装的json
     * */
    @PutMapping
    public Result update(@RequestBody ReportingProcess reportingProcess){
        boolean flag = reportingProcessService.updateById(reportingProcess);
        Integer code = flag ? Code.UPDATE_OK : Code.UPDATE_ERR;
        String msg = flag ? "数据更新成功" : "数据更新失败，请重试！";
        return new Result(code, flag, msg);
    }

    /**
     * 根据id删除报道流程数据
     *
     * @param id 删除id
     * @return Result型封装的json
     * */
    @DeleteMapping("{id}")
    public Result delete(@PathVariable Integer id) {
        boolean flag = reportingProcessService.removeById(id);
        Integer code = flag ? Code.DELETE_OK : Code.DELETE_ERR;
        String msg = flag ? "数据删除成功" : "数据删除失败，请重试！";
        return new Result(code, flag, msg);
    }

    /**
     * 根据id查询报道流程的请求
     *
     * @param id 查询id
     * @return Result型封装的json
     */
    @GetMapping("{id}")
    public Result getById(@PathVariable Integer id){
        ReportingProcess reportingProcess = reportingProcessService.getById(id);
        Integer code = reportingProcess != null ? Code.GET_OK : Code.GET_ERR;
        String msg = reportingProcess != null ? "数据查询成功" : "数据查询失败，请重试！";
        return new Result(code, reportingProcess, msg);
    }

    /**
     * 分页查询，提供基于条件的分页查询
     *
     * @param currentPage 当前页
     * @param pageSize    分页大小
     * @param reportingProcess     查询条件，默认为空
     * @return Result型封装的json
     */
    @GetMapping("{currentPage}/{pageSize}")
    public Result getPage(@PathVariable int currentPage, @PathVariable int pageSize, ReportingProcess reportingProcess){
        IPage<ReportingProcess> page = reportingProcessService.getPage(currentPage, pageSize, reportingProcess);
        if(currentPage > page.getPages()){
            page = reportingProcessService.getPage(currentPage, pageSize, reportingProcess);
        }
        Integer code = page != null ? Code.GET_OK : Code.GET_ERR;
        String msg = page != null ? "分页查询成功" : "分页查询失败，请重试！";
        return new Result(code, page, msg);
    }
}

