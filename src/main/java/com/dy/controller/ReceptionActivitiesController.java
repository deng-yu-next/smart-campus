package com.dy.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.dy.domain.ReceptionActivities;
import com.dy.service.IReceptionActivitiesService;
import com.dy.utils.Code;
import com.dy.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author dengyu
 * @since 2023-10-13
 */
@RestController
@RequestMapping("/reception")
public class ReceptionActivitiesController {
    @Autowired
    private IReceptionActivitiesService receptionActivitiesService;

    /**
     * 查询所有接待活动数据
     *
     * @return Result型封装的json
     * */
    @GetMapping
    public Result getALL(){
        List<ReceptionActivities> receptionActivities = receptionActivitiesService.list();
        Integer code = receptionActivities != null ? Code.GET_OK : Code.GET_ERR;
        String msg = receptionActivities != null ? "数据库查询成功" : "数据查询失败，请重试！";
        return new Result(code, receptionActivities, msg);
    }

    /**
     * 添加接待活动数据
     *
     * @param receptionActivities 提交的接待活动数据
     * @return Result型封装的json
     * */
    @PostMapping
    public Result sava(@RequestBody ReceptionActivities receptionActivities){
        boolean flag = receptionActivitiesService.save(receptionActivities);
        Integer code = flag ? Code.SAVE_OK : Code.SAVE_ERR;
        String msg = flag ? "数据添加成功" : "数据添加失败，请重试！";
        return new Result(code, flag, msg);
    }

    /**
     * 更新接待活动数据
     *
     * @param receptionActivities 提交的接待活动更新数据
     * @return Result型封装的json
     * */
    @PutMapping
    public Result update(@RequestBody ReceptionActivities receptionActivities){
        boolean flag = receptionActivitiesService.updateById(receptionActivities);
        Integer code = flag ? Code.UPDATE_OK : Code.UPDATE_ERR;
        String msg = flag ? "数据更新成功" : "数据更新失败，请重试！";
        return new Result(code, flag, msg);
    }

    /**
     * 根据id删除接待活动数据
     *
     * @param id 删除id
     * @return Result型封装的json
     * */
    @DeleteMapping("{id}")
    public Result delete(@PathVariable Integer id) {
        boolean flag = receptionActivitiesService.removeById(id);
        Integer code = flag ? Code.DELETE_OK : Code.DELETE_ERR;
        String msg = flag ? "数据删除成功" : "数据删除失败，请重试！";
        return new Result(code, flag, msg);
    }

    /**
     * 根据id查询接待活动的请求
     *
     * @param id 查询id
     * @return Result型封装的json
     */
    @GetMapping("{id}")
    public Result getById(@PathVariable Integer id){
        ReceptionActivities receptionActivities = receptionActivitiesService.getById(id);
        Integer code = receptionActivities != null ? Code.GET_OK : Code.GET_ERR;
        String msg = receptionActivities != null ? "数据查询成功" : "数据查询失败，请重试！";
        return new Result(code, receptionActivities, msg);
    }

    /**
     * 分页查询，提供基于条件的分页查询
     *
     * @param currentPage 当前页
     * @param pageSize    分页大小
     * @param receptionActivities     查询条件，默认为空
     * @return Result型封装的json
     */
    @GetMapping("{currentPage}/{pageSize}")
    public Result getPage(@PathVariable int currentPage, @PathVariable int pageSize, ReceptionActivities receptionActivities){
        IPage<ReceptionActivities> page = receptionActivitiesService.getPage(currentPage, pageSize, receptionActivities);
        if(currentPage > page.getPages()){
            page = receptionActivitiesService.getPage(currentPage, pageSize, receptionActivities);
        }
        Integer code = page != null ? Code.GET_OK : Code.GET_ERR;
        String msg = page != null ? "分页查询成功" : "分页查询失败，请重试！";
        return new Result(code, page, msg);
    }
}

