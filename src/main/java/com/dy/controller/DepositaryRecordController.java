package com.dy.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.dy.domain.DepositaryRecord;
import com.dy.service.IDepositaryRecordService;
import com.dy.utils.Code;
import com.dy.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;
/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author dengyu
 * @since 2023-10-13
 */
@RestController
@RequestMapping("/depositary")
public class DepositaryRecordController {

    @Autowired
    private IDepositaryRecordService depositaryRecordService;

    /**
     * 查询所有寄存记录数据
     *
     * @return Result型封装的json
     */
    @GetMapping
    public Result getAll() {
        List<DepositaryRecord> depositaryRecords = depositaryRecordService.list();
        Integer code = depositaryRecords != null ? Code.GET_OK : Code.GET_ERR;
        String msg = depositaryRecords != null ? "数据查询成功" : "数据查询失败，请重试！";
        return new Result(code, depositaryRecords, msg);
    }

    /**
     * 添加新的寄存记录的请求
     *
     * @param depositaryRecord 提交的寄存记录更新数据
     * @return Result型封装的json
     */
    @PostMapping
    public Result save(@RequestBody DepositaryRecord depositaryRecord) {
        boolean flag = depositaryRecordService.save(depositaryRecord);
        Integer code = flag ? Code.SAVE_OK : Code.SAVE_ERR;
        String msg = flag ? "数据添加成功" : "数据添加失败，请重试！";
        return new Result(code, flag, msg);
    }

    /**
     * 更新寄存记录的请求
     *
     * @param depositaryRecord 提交的寄存记录更新数据
     * @return Result型封装的json
     */
    @PutMapping
    public Result update(@RequestBody DepositaryRecord depositaryRecord) {
        boolean flag = depositaryRecordService.updateById(depositaryRecord);
        Integer code = flag ? Code.UPDATE_OK : Code.UPDATE_ERR;
        String msg = flag ? "数据更新成功" : "数据更新失败，请重试！";
        return new Result(code, flag, msg);
    }

    /**
     * 根据id删除寄存记录的请求
     *
     * @param id 删除id
     * @return Result型封装的json
     */
    @DeleteMapping("{id}")
    public Result delete(@PathVariable Integer id) {
        boolean flag = depositaryRecordService.removeById(id);
        Integer code = flag ? Code.DELETE_OK : Code.DELETE_ERR;
        String msg = flag ? "数据删除成功" : "数据删除失败，请重试！";
        return new Result(code, flag, msg);
    }

    /**
     * 根据id查询寄存记录信息的请求
     *
     * @param id 查询id
     * @return Result型封装的json
     */
    @GetMapping("{id}")
    public Result getById(@PathVariable Integer id) {
        DepositaryRecord depositaryRecord = depositaryRecordService.getById(id);
        Integer code = depositaryRecord != null ? Code.GET_OK : Code.GET_ERR;
        String msg = depositaryRecord != null ? "数据查询成功" : "数据查询失败，请重试！";
        return new Result(code, depositaryRecord, msg);
    }

    /**
     * 分页查询，提供基于条件的分页查询
     *
     * @param currentPage 当前业
     * @param pageSize    分页大小
     * @param depositaryRecord    查询条件，默认为空
     * @return Result型封装的json
     */
    @GetMapping("{currentPage}/{pageSize}")
    public Result getPage(@PathVariable int currentPage, @PathVariable int pageSize, DepositaryRecord depositaryRecord) {


        IPage<DepositaryRecord> page = depositaryRecordService.getPage(currentPage, pageSize, depositaryRecord);
        //如果当前页码值大于了总页码值，那么重新执行查询操作，使用最大页码值作为当前页码值
        if (currentPage > page.getPages()) {
            page = depositaryRecordService.getPage(currentPage, pageSize, depositaryRecord);
        }
        Integer code = page != null ? Code.GET_OK : Code.GET_ERR;
        String msg = page != null ? "分页查询成功" : "分页查询失败，请重试！";
        return new Result(code, page, msg);
    }
}

