package com.dy.controller;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.dy.domain.Student;
import com.dy.service.IStudentService;
import com.dy.utils.Code;
import com.dy.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author dengyu
 * @since 2023-10-13
 */
@RestController
@RequestMapping("/student")
public class StudentController {
    @Autowired
    private IStudentService studentService;

    /**
     * 查询所有student数据
     *
     * @return Result型封装的json
     */
    @GetMapping
    public Result getAll() {
        List<Student> students = studentService.list();
        Integer code = students != null ? Code.GET_OK : Code.GET_ERR;
        String msg = students != null ? "数据查询成功" : "数据查询失败，请重试！";
        return new Result(code, students, msg);
    }

    /**
     * 添加新的student的请求
     *
     * @param student 提交的student更新数据
     * @return Result型封装的json
     */
    @PostMapping
    public Result save(@RequestBody Student student) {
        boolean flag = studentService.save(student);
        Integer code = flag ? Code.SAVE_OK : Code.SAVE_ERR;
        String msg = flag ? "数据添加成功" : "数据添加失败，请重试！";
        return new Result(code, flag, msg);
    }

    /**
     * 更新student的请求
     *
     * @param student 提交的student更新数据
     * @return Result型封装的json
     */
    @PutMapping
    public Result update(@RequestBody Student student) {
        boolean flag = studentService.updateById(student);
        Integer code = flag ? Code.UPDATE_OK : Code.UPDATE_ERR;
        String msg = flag ? "数据更新成功" : "数据更新失败，请重试！";
        return new Result(code, flag, msg);
    }

    /**
     * 根据id删除student的请求
     *
     * @param id 删除id
     * @return Result型封装的json
     */
    @DeleteMapping("{id}")
    public Result delete(@PathVariable Integer id) {
        boolean flag = studentService.removeById(id);
        Integer code = flag ? Code.DELETE_OK : Code.DELETE_ERR;
        String msg = flag ? "数据删除成功" : "数据删除失败，请重试！";
        return new Result(code, flag, msg);
    }

    /**
     * 根据id查询信息的请求
     *
     * @param id 查询id
     * @return Result型封装的json
     */
    @GetMapping("{id}")
    public Result getById(@PathVariable Integer id) {
        Student student = studentService.getById(id);
        Integer code = student != null ? Code.GET_OK : Code.GET_ERR;
        String msg = student != null ? "数据查询成功" : "数据查询失败，请重试！";
        return new Result(code, student, msg);
    }

    /**
     * 分页查询，提供基于条件的分页查询
     *
     * @param currentPage 当前业
     * @param pageSize    分页大小
     * @param student     查询条件，默认为空
     * @return Result型封装的json
     */
    @GetMapping("{currentPage}/{pageSize}")
    public Result getPage(@PathVariable int currentPage, @PathVariable int pageSize, Student student) {


        IPage<Student> page = studentService.getPage(currentPage, pageSize, student);
        //如果当前页码值大于了总页码值，那么重新执行查询操作，使用最大页码值作为当前页码值
        if (currentPage > page.getPages()) {
            page = studentService.getPage(currentPage, pageSize, student);
        }
        Integer code = page != null ? Code.GET_OK : Code.GET_ERR;
        String msg = page != null ? "分页查询成功" : "分页查询失败，请重试！";
        return new Result(code, page, msg);
    }

    /**
     * 用户登录
     *
     * @param studentNumber String 学号
     * @param password      String 密码
     * @return Result型封装的json
     */
    @GetMapping("/login/{studentNumber}/{password}")
    public Result login(@PathVariable String studentNumber, @PathVariable String password) {
        LambdaQueryWrapper<Student> lqw = new LambdaQueryWrapper<Student>();
        lqw.like( Student::getStudentNumber, studentNumber);
        Student student = studentService.getOne(lqw);
        Integer code = Code.GET_ERR;
        if (student == null) {
            String msg = "学号不存在，请重试！";
            return new Result(code, "error", msg);
        }
        if (password.equals(student.getPassword())) {
            code = Code.GET_OK;
            String msg = "登录成功";
            return new Result(code, student, msg);
        }else{
            String msg = "密码错误，请重试！";
            return new Result(code, "error", msg);
        }
    }
}

