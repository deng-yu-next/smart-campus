package com.dy.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.dy.domain.Punishment;
import com.dy.service.IPunishmentService;
import com.dy.utils.Code;
import com.dy.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author dengyu
 * @since 2023-10-13
 */
@RestController
@RequestMapping("/punishment")
public class PunishmentController {
    @Autowired
    private IPunishmentService punishmentService;

    /**
     * 查询所有处分数据
     *
     * @return Result型封装的json
     * */
    @GetMapping
    public Result getALL(){
        List<Punishment> punishments = punishmentService.list();
        Integer code = punishments != null ? Code.GET_OK : Code.GET_ERR;
        String msg = punishments != null ? "数据库查询成功" : "数据查询失败，请重试！";
        return new Result(code, punishments, msg);
    }

    /**
     * 添加处分数据
     *
     * @param punishment 提交的处分数据
     * @return Result型封装的json
     * */
    @PostMapping
    public Result sava(@RequestBody Punishment punishment){
        boolean flag = punishmentService.save(punishment);
        Integer code = flag ? Code.SAVE_OK : Code.SAVE_ERR;
        String msg = flag ? "数据添加成功" : "数据添加失败，请重试！";
        return new Result(code, flag, msg);
    }

    /**
     * 更新处分数据
     *
     * @param punishment 提交的处分更新数据
     * @return Result型封装的json
     * */
    @PutMapping
    public Result update(@RequestBody Punishment punishment){
        boolean flag = punishmentService.updateById(punishment);
        Integer code = flag ? Code.UPDATE_OK : Code.UPDATE_ERR;
        String msg = flag ? "数据更新成功" : "数据更新失败，请重试！";
        return new Result(code, flag, msg);
    }

    /**
     * 根据id删除处分数据
     *
     * @param id 删除id
     * @return Result型封装的json
     * */
    @DeleteMapping("{id}")
    public Result delete(@PathVariable Integer id) {
        boolean flag = punishmentService.removeById(id);
        Integer code = flag ? Code.DELETE_OK : Code.DELETE_ERR;
        String msg = flag ? "数据删除成功" : "数据删除失败，请重试！";
        return new Result(code, flag, msg);
    }

    /**
     * 根据id查询处分的请求
     *
     * @param id 查询id
     * @return Result型封装的json
     */
    @GetMapping("{id}")
    public Result getById(@PathVariable Integer id){
        Punishment punishment = punishmentService.getById(id);
        Integer code = punishment != null ? Code.GET_OK : Code.GET_ERR;
        String msg = punishment != null ? "数据查询成功" : "数据查询失败，请重试！";
        return new Result(code, punishment, msg);
    }

    /**
     * 分页查询，提供基于条件的分页查询
     *
     * @param currentPage 当前页
     * @param pageSize    分页大小
     * @param punishment     查询条件，默认为空
     * @return Result型封装的json
     */
    @GetMapping("{currentPage}/{pageSize}")
    public Result getPage(@PathVariable int currentPage, @PathVariable int pageSize, Punishment punishment){
        IPage<Punishment> page = punishmentService.getPage(currentPage, pageSize, punishment);
        if(currentPage > page.getPages()){
            page = punishmentService.getPage(currentPage, pageSize, punishment);
        }
        Integer code = page != null ? Code.GET_OK : Code.GET_ERR;
        String msg = page != null ? "分页查询成功" : "分页查询失败，请重试！";
        return new Result(code, page, msg);
    }
}

