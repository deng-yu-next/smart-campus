package com.dy.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.dy.domain.NetworkService;
import com.dy.service.INetworkServiceService;
import com.dy.utils.Code;
import com.dy.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author dengyu
 * @since 2023-10-13
 */
@RestController
@RequestMapping("/network")
public class NetworkServiceController {
    @Autowired
    private INetworkServiceService networkServiceService;

    /**
     * 查询所有网络服务数据
     *
     * @return Result型封装的json
     * */
    @GetMapping
    public Result getALL(){
        List<NetworkService> networkServices = networkServiceService.list();
        Integer code = networkServices != null ? Code.GET_OK : Code.GET_ERR;
        String msg = networkServices != null ? "数据库查询成功" : "数据查询失败，请重试！";
        return new Result(code, networkServices, msg);
    }

    /**
     * 添加网络服务数据
     *
     * @param networkService 提交的网络服务数据
     * @return Result型封装的json
     * */
    @PostMapping
    public Result sava(@RequestBody NetworkService networkService){
        boolean flag = networkServiceService.save(networkService);
        Integer code = flag ? Code.SAVE_OK : Code.SAVE_ERR;
        String msg = flag ? "数据添加成功" : "数据添加失败，请重试！";
        return new Result(code, flag, msg);
    }

    /**
     * 更新网络服务数据
     *
     * @param networkService 提交的网络服务更新数据
     * @return Result型封装的json
     * */
    @PutMapping
    public Result update(@RequestBody NetworkService networkService){
        boolean flag = networkServiceService.updateById(networkService);
        Integer code = flag ? Code.UPDATE_OK : Code.UPDATE_ERR;
        String msg = flag ? "数据更新成功" : "数据更新失败，请重试！";
        return new Result(code, flag, msg);
    }

    /**
     * 根据id网络服务数据
     *
     * @param id 删除id
     * @return Result型封装的json
     * */
    @DeleteMapping("{id}")
    public Result delete(@PathVariable Integer id) {
        boolean flag = networkServiceService.removeById(id);
        Integer code = flag ? Code.DELETE_OK : Code.DELETE_ERR;
        String msg = flag ? "数据删除成功" : "数据删除失败，请重试！";
        return new Result(code, flag, msg);
    }

    /**
     * 根据id查询网络服务的请求
     *
     * @param id 查询id
     * @return Result型封装的json
     */
    @GetMapping("{id}")
    public Result getById(@PathVariable Integer id){
        NetworkService networkService = networkServiceService.getById(id);
        Integer code = networkService != null ? Code.GET_OK : Code.GET_ERR;
        String msg = networkService != null ? "数据查询成功" : "数据查询失败，请重试！";
        return new Result(code, networkService, msg);
    }

    /**
     * 分页查询，提供基于条件的分页查询
     *
     * @param currentPage 当前页
     * @param pageSize    分页大小
     * @param networkService     查询条件，默认为空
     * @return Result型封装的json
     */
    @GetMapping("{currentPage}/{pageSize}")
    public Result getPage(@PathVariable int currentPage, @PathVariable int pageSize, NetworkService networkService){
        IPage<NetworkService> page = networkServiceService.getPage(currentPage, pageSize, networkService);

        if(currentPage > page.getPages()){
            page = networkServiceService.getPage(currentPage, pageSize, networkService);
        }
        Integer code = page != null ? Code.GET_OK : Code.GET_ERR;
        String msg = page != null ? "分页查询成功" : "分页查询失败，请重试！";
        return new Result(code, page, msg);
    }
}

