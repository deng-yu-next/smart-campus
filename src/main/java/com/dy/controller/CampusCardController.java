package com.dy.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.dy.domain.CampusCard;
import com.dy.service.ICampusCardService;
import com.dy.utils.Code;
import com.dy.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author dengyu
 * @since 2023-10-13
 */
@RestController
@RequestMapping("/campus")
public class CampusCardController {

    @Autowired
    private ICampusCardService campusCardService;

    /**
     * 查询所有校园卡数据
     *
     * @return Result型封装的json
     */
    @GetMapping
    public Result getAll() {
        List<CampusCard> campusCards = campusCardService.list();
        Integer code = campusCards != null ? Code.GET_OK : Code.GET_ERR;
        String msg = campusCards != null ? "数据查询成功" : "数据查询失败，请重试！";
        return new Result(code, campusCards, msg);
    }

    /**
     * 添加新的校园卡的请求
     *
     * @param campusCard 提交的校园卡更新数据
     * @return Result型封装的json
     */
    @PostMapping
    public Result save(@RequestBody CampusCard campusCard) {
        boolean flag = campusCardService.save(campusCard);
        Integer code = flag ? Code.SAVE_OK : Code.SAVE_ERR;
        String msg = flag ? "数据添加成功" : "数据添加失败，请重试！";
        return new Result(code, flag, msg);
    }

    /**
     * 更新校园卡的请求
     *
     * @param campusCard 提交的校园卡更新数据
     * @return Result型封装的json
     */
    @PutMapping
    public Result update(@RequestBody CampusCard campusCard) {
        boolean flag = campusCardService.updateById(campusCard);
        Integer code = flag ? Code.UPDATE_OK : Code.UPDATE_ERR;
        String msg = flag ? "数据更新成功" : "数据更新失败，请重试！";
        return new Result(code, flag, msg);
    }

    /**
     * 根据id删除校园卡的请求
     *
     * @param id 删除id
     * @return Result型封装的json
     */
    @DeleteMapping("{id}")
    public Result delete(@PathVariable Integer id) {
        boolean flag = campusCardService.removeById(id);
        Integer code = flag ? Code.DELETE_OK : Code.DELETE_ERR;
        String msg = flag ? "数据删除成功" : "数据删除失败，请重试！";
        return new Result(code, flag, msg);
    }

    /**
     * 根据id查询信息的请求
     *
     * @param id 查询id
     * @return Result型封装的json
     */
    @GetMapping("{id}")
    public Result getById(@PathVariable Integer id) {
        CampusCard campusCard = campusCardService.getById(id);
        Integer code = campusCard != null ? Code.GET_OK : Code.GET_ERR;
        String msg = campusCard != null ? "数据查询成功" : "数据查询失败，请重试！";
        return new Result(code, campusCard, msg);
    }

    /**
     * 分页查询，提供基于条件的分页查询
     *
     * @param currentPage 当前业
     * @param pageSize    分页大小
     * @param campusCard     查询条件，默认为空
     * @return Result型封装的json
     */
    @GetMapping("{currentPage}/{pageSize}")
    public Result getPage(@PathVariable int currentPage, @PathVariable int pageSize, CampusCard campusCard) {


        IPage<CampusCard> page = campusCardService.getPage(currentPage, pageSize, campusCard);
        //如果当前页码值大于了总页码值，那么重新执行查询操作，使用最大页码值作为当前页码值
        if (currentPage > page.getPages()) {
            page = campusCardService.getPage(currentPage, pageSize, campusCard);
        }
        Integer code = page != null ? Code.GET_OK : Code.GET_ERR;
        String msg = page != null ? "分页查询成功" : "分页查询失败，请重试！";
        return new Result(code, page, msg);
    }
}
