package com.dy.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.dy.domain.StudentStatus;
import com.dy.service.IStudentStatusService;
import com.dy.utils.Code;
import com.dy.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author dengyu
 * @since 2023-10-13
 */
@RestController
@RequestMapping("/student/status")
public class StudentStatusController {
    @Autowired
    private IStudentStatusService studentStatusService;

    /**
     * 查询所有学生状态数据
     *
     * @return Result型封装的json
     * */
    @GetMapping
    public Result getALL(){
        List<StudentStatus> studentStatuses = studentStatusService.list();
        Integer code = studentStatuses != null ? Code.GET_OK : Code.GET_ERR;
        String msg = studentStatuses != null ? "数据库查询成功" : "数据查询失败，请重试！";
        return new Result(code, studentStatuses, msg);
    }

    /**
     * 添加学生状态数据
     *
     * @param studentStatus 提交的学生状态数据
     * @return Result型封装的json
     * */
    @PostMapping
    public Result sava(@RequestBody StudentStatus studentStatus){
        boolean flag = studentStatusService.save(studentStatus);
        Integer code = flag ? Code.SAVE_OK : Code.SAVE_ERR;
        String msg = flag ? "数据添加成功" : "数据添加失败，请重试！";
        return new Result(code, flag, msg);
    }

    /**
     * 更新学生状态数据
     *
     * @param studentStatus 提交的学生状态更新数据
     * @return Result型封装的json
     * */
    @PutMapping
    public Result update(@RequestBody StudentStatus studentStatus){
        boolean flag = studentStatusService.updateById(studentStatus);
        Integer code = flag ? Code.UPDATE_OK : Code.UPDATE_ERR;
        String msg = flag ? "数据更新成功" : "数据更新失败，请重试！";
        return new Result(code, flag, msg);
    }

    /**
     * 根据id删除学生状态数据
     *
     * @param id 删除id
     * @return Result型封装的json
     * */
    @DeleteMapping("{id}")
    public Result delete(@PathVariable Integer id) {
        boolean flag = studentStatusService.removeById(id);
        Integer code = flag ? Code.DELETE_OK : Code.DELETE_ERR;
        String msg = flag ? "数据删除成功" : "数据删除失败，请重试！";
        return new Result(code, flag, msg);
    }

    /**
     * 根据id查询学生状态的请求
     *
     * @param id 查询id
     * @return Result型封装的json
     */
    @GetMapping("{id}")
    public Result getById(@PathVariable Integer id){
        StudentStatus studentStatus = studentStatusService.getById(id);
        Integer code = studentStatus != null ? Code.GET_OK : Code.GET_ERR;
        String msg = studentStatus != null ? "数据查询成功" : "数据查询失败，请重试！";
        return new Result(code, studentStatus, msg);
    }

    /**
     * 分页查询，提供基于条件的分页查询
     *
     * @param currentPage 当前页
     * @param pageSize    分页大小
     * @param studentStatus     查询条件，默认为空
     * @return Result型封装的json
     */
    @GetMapping("{currentPage}/{pageSize}")
    public Result getPage(@PathVariable int currentPage, @PathVariable int pageSize, StudentStatus studentStatus){
        IPage<StudentStatus> page = studentStatusService.getPage(currentPage, pageSize, studentStatus);
        if(currentPage > page.getPages()){
            page = studentStatusService.getPage(currentPage, pageSize, studentStatus);
        }
        Integer code = page != null ? Code.GET_OK : Code.GET_ERR;
        String msg = page != null ? "分页查询成功" : "分页查询失败，请重试！";
        return new Result(code, page, msg);
    }
}

