package com.dy.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.dy.domain.LeavingSchool;
import com.dy.service.ILeavingSchoolService;
import com.dy.utils.Code;
import com.dy.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author dengyu
 * @since 2023-10-13
 */
@RestController
@RequestMapping("/leaving")
public class LeavingSchoolController {
    @Autowired
    private ILeavingSchoolService leavingSchoolService;

    /**
     * 查询所有离校数据
     *
     * @return Result型封装的json
     * */
    @GetMapping
    public Result getALL(){
        List<LeavingSchool> leavingSchools = leavingSchoolService.list();
        Integer code = leavingSchools != null ? Code.GET_OK : Code.GET_ERR;
        String msg = leavingSchools != null ? "数据库查询成功" : "数据查询失败，请重试！";
        return new Result(code, leavingSchools, msg);
    }

    /**
     * 添加离校数据
     *
     * @param leavingSchool 提交的假期留校数据
     * @return Result型封装的json
     * */
    @PostMapping
    public Result sava(@RequestBody LeavingSchool leavingSchool){
        boolean flag = leavingSchoolService.save(leavingSchool);
        Integer code = flag ? Code.SAVE_OK : Code.SAVE_ERR;
        String msg = flag ? "数据添加成功" : "数据添加失败，请重试！";
        return new Result(code, flag, msg);
    }

    /**
     * 更新离校数据
     *
     * @param leavingSchool 提交的假期留校更新数据
     * @return Result型封装的json
     * */
    @PutMapping
    public Result update(@RequestBody LeavingSchool leavingSchool){
        boolean flag = leavingSchoolService.updateById(leavingSchool);
        Integer code = flag ? Code.UPDATE_OK : Code.UPDATE_ERR;
        String msg = flag ? "数据更新成功" : "数据更新失败，请重试！";
        return new Result(code, flag, msg);
    }

    /**
     * 根据id删除离校数据
     *
     * @param id 删除id
     * @return Result型封装的json
     * */
    @DeleteMapping("{id}")
    public Result delete(@PathVariable Integer id) {
        boolean flag = leavingSchoolService.removeById(id);
        Integer code = flag ? Code.DELETE_OK : Code.DELETE_ERR;
        String msg = flag ? "数据删除成功" : "数据删除失败，请重试！";
        return new Result(code, flag, msg);
    }

    /**
     * 根据id查询离校信息的请求
     *
     * @param id 查询id
     * @return Result型封装的json
     */
    @GetMapping("{id}")
    public Result getById(@PathVariable Integer id){
        LeavingSchool leavingSchool = leavingSchoolService.getById(id);
        Integer code = leavingSchool != null ? Code.GET_OK : Code.GET_ERR;
        String msg = leavingSchool != null ? "数据查询成功" : "数据查询失败，请重试！";
        return new Result(code, leavingSchool, msg);
    }

    /**
     * 分页查询，提供基于条件的分页查询
     *
     * @param currentPage 当前页
     * @param pageSize    分页大小
     * @param leavingSchool     查询条件，默认为空
     * @return Result型封装的json
     */
    @GetMapping("{currentPage}/{pageSize}")
    public Result getPage(@PathVariable int currentPage, @PathVariable int pageSize, LeavingSchool leavingSchool){
        IPage<LeavingSchool> page = leavingSchoolService.getPage(currentPage, pageSize, leavingSchool);
        if(currentPage > page.getPages()){
            page = leavingSchoolService.getPage(currentPage, pageSize, leavingSchool);
        }
        Integer code = page != null ? Code.GET_OK : Code.GET_ERR;
        String msg = page != null ? "分页查询成功" : "分页查询失败，请重试！";
        return new Result(code, page, msg);
    }
}

