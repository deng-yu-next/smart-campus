package com.dy.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dy.domain.NetworkService;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author dengyu
 * @since 2023-10-13
 */
@Mapper
public interface NetworkServiceDao extends BaseMapper<NetworkService> {

}
