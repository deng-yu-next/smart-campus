package com.dy.utils;

public class Code {
    public static final Integer SAVE_OK = 201;
    public static final Integer DELETE_OK = 202;
    public static final Integer UPDATE_OK = 203;
    public static final Integer GET_OK = 204;

    public static final Integer SAVE_ERR = 301;
    public static final Integer DELETE_ERR = 302;
    public static final Integer UPDATE_ERR = 303;
    public static final Integer GET_ERR = 304;

    public static final Integer SYSTEM_ERR = 501;
    public static final Integer SYSTEM_TIMEOUT_ERR = 502;
    public static final Integer SYSTEM_UNKNOW_ERR = 509;

    public static final Integer BUSINESS_ERR = 601;



}
