package com.dy.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.dy.domain.LeavingSchool;
import com.dy.domain.WelcomeService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author dengyu
 * @since 2023-10-13
 */
public interface IWelcomeServiceService extends IService<WelcomeService> {
    IPage<WelcomeService> getPage(int currentPage, int pageSize);

    IPage<WelcomeService> getPage(int currentPage, int pageSize, WelcomeService welcomeService);
}
