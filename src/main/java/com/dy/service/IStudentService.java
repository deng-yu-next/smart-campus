package com.dy.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.dy.domain.Student;

public interface IStudentService extends IService<Student> {

    IPage<Student> getPage(int currentPage, int pageSize);
    IPage<Student> getPage(int currentPage, int pageSize, Student student);

}
