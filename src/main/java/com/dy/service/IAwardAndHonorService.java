package com.dy.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.dy.domain.AwardAndHonor;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author dengyu
 * @since 2023-10-13
 */
public interface IAwardAndHonorService extends IService<AwardAndHonor> {
    IPage<AwardAndHonor> getPage(int currentPage, int pageSize);

    IPage<AwardAndHonor> getPage(int currentPage, int pageSize, AwardAndHonor awardAndHonor);
}
