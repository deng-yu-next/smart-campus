package com.dy.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.dy.domain.FinancialAssistance;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author dengyu
 * @since 2023-10-13
 */
public interface IFinancialAssistanceService extends IService<FinancialAssistance> {

    IPage<FinancialAssistance> getPage(int currentPage, int pageSize);
    IPage<FinancialAssistance> getPage(int currentPage, int pageSize, FinancialAssistance FinancialAssistance);
}
