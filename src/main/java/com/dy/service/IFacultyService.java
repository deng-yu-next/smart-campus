package com.dy.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.dy.domain.Faculty;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author dengyu
 * @since 2023-10-13
 */
public interface IFacultyService extends IService<Faculty> {

    IPage<Faculty> getPage(int currentPage, int pageSize);
    IPage<Faculty> getPage(int currentPage, int pageSize, Faculty Faculty);
}
