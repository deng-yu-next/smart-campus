package com.dy.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.dy.domain.DormitoryRepairRecord;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author dengyu
 * @since 2023-10-13
 */
public interface IDormitoryRepairRecordService extends IService<DormitoryRepairRecord> {

    IPage<DormitoryRepairRecord> getPage(int currentPage, int pageSize);
    IPage<DormitoryRepairRecord> getPage(int currentPage, int pageSize, DormitoryRepairRecord DormitoryRepairRecord);
}
