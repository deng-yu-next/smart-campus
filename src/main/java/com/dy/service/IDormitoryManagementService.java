package com.dy.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.dy.domain.DormitoryManagement;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author dengyu
 * @since 2023-10-13
 */ 
public interface IDormitoryManagementService extends IService<DormitoryManagement> {
    IPage<DormitoryManagement> getPage(int currentPage, int pageSize);
    IPage<DormitoryManagement> getPage(int currentPage, int pageSize, DormitoryManagement DormitoryManagement);
}
