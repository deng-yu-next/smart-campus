package com.dy.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.dy.domain.LeavingSchool;
import com.dy.domain.StudentStatus;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author dengyu
 * @since 2023-10-13
 */
public interface IStudentStatusService extends IService<StudentStatus> {
    IPage<StudentStatus> getPage(int currentPage, int pageSize);

    IPage<StudentStatus> getPage(int currentPage, int pageSize, StudentStatus studentStatus);
}
