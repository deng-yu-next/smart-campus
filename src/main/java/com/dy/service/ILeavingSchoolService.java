package com.dy.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.dy.domain.LeavingSchool;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author dengyu
 * @since 2023-10-13
 */
public interface ILeavingSchoolService extends IService<LeavingSchool> {
    IPage<LeavingSchool> getPage(int currentPage, int pageSize);

    IPage<LeavingSchool> getPage(int currentPage, int pageSize, LeavingSchool leavingSchool);
}
