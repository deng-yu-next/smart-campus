package com.dy.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.dy.domain.DormitoryDisciplinaryRecord;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author dengyu
 * @since 2023-10-13
 */
public interface IDormitoryDisciplinaryRecordService extends IService<DormitoryDisciplinaryRecord> {

    IPage<DormitoryDisciplinaryRecord> getPage(int currentPage, int pageSize);
    IPage<DormitoryDisciplinaryRecord> getPage(int currentPage, int pageSize, DormitoryDisciplinaryRecord DormitoryDisciplinaryRecord);
}
