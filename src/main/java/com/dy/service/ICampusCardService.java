package com.dy.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.dy.domain.CampusCard;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author dengyu
 * @since 2023-10-13
 */
public interface ICampusCardService extends IService<CampusCard> {
    IPage<CampusCard> getPage(int currentPage, int pageSize);

    IPage<CampusCard> getPage(int currentPage, int pageSize, CampusCard awardAndHonor);
}
