package com.dy.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.dy.domain.CheckIn;
import com.dy.domain.DepositaryRecord;
import com.dy.domain.DepositaryRecord;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author dengyu
 * @since 2023-10-13
 */
public interface IDepositaryRecordService extends IService<DepositaryRecord> {
    IPage<DepositaryRecord> getPage(int currentPage, int pageSize);
    IPage<DepositaryRecord> getPage(int currentPage, int pageSize, DepositaryRecord depositaryRecord);
}
