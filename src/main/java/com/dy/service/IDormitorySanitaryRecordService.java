package com.dy.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.dy.domain.DormitorySanitaryRecord;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author dengyu
 * @since 2023-10-13
 */
public interface IDormitorySanitaryRecordService extends IService<DormitorySanitaryRecord> {

    IPage<DormitorySanitaryRecord> getPage(int currentPage, int pageSize);
    IPage<DormitorySanitaryRecord> getPage(int currentPage, int pageSize, DormitorySanitaryRecord DormitorySanitaryRecord);
}
