package com.dy.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dy.dao.FinancialAssistanceDao;
import com.dy.dao.FinancialAssistanceDao;
import com.dy.domain.FinancialAssistance;
import com.dy.domain.FinancialAssistance;
import com.dy.service.IFinancialAssistanceService;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author dengyu
 * @since 2023-10-13
 */
@Service
public class FinancialAssistanceServiceImpl extends ServiceImpl<FinancialAssistanceDao, FinancialAssistance> implements IFinancialAssistanceService {

    @Autowired
    private FinancialAssistanceDao financialAssistanceDao;

    @Override
    public IPage<FinancialAssistance> getPage(int currentPage, int pageSize) {
        IPage page = new Page(currentPage, pageSize);
        financialAssistanceDao.selectPage(page, null);
        return page;
    }
    @Override
    public IPage<FinancialAssistance> getPage(int currentPage, int pageSize, FinancialAssistance financialAssistance) {
        LambdaQueryWrapper<FinancialAssistance> lqw = new LambdaQueryWrapper<FinancialAssistance>();
        lqw.like(Strings.isNotEmpty(financialAssistance.getStudentNumber()), FinancialAssistance::getStudentNumber, financialAssistance.getStudentNumber());
        lqw.like(true, FinancialAssistance::getApprovalStatus, financialAssistance.getApprovalStatus());
        lqw.like(true, FinancialAssistance::getClass, financialAssistance.getClass());
        IPage page = new Page(currentPage, pageSize);
        financialAssistanceDao.selectPage(page, lqw);
        return page;
    }
}
