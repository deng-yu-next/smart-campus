package com.dy.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dy.dao.CheckInDao;
import com.dy.dao.DepositaryRecordDao;
import com.dy.domain.CheckIn;
import com.dy.domain.DepositaryRecord;
import com.dy.service.IDepositaryRecordService;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author dengyu
 * @since 2023-10-13
 */
@Service
public class DepositaryRecordServiceImpl extends ServiceImpl<DepositaryRecordDao, DepositaryRecord> implements IDepositaryRecordService {

    @Autowired
    private DepositaryRecordDao depositaryRecordDao;

    @Override
    public IPage<DepositaryRecord> getPage(int currentPage, int pageSize) {
        IPage page = new Page(currentPage, pageSize);
        depositaryRecordDao.selectPage(page, null);
        return page;
    }
    @Override
    public IPage<DepositaryRecord> getPage(int currentPage, int pageSize, DepositaryRecord depositaryRecord) {
        LambdaQueryWrapper<DepositaryRecord> lqw = new LambdaQueryWrapper<DepositaryRecord>();
        lqw.like(Strings.isNotEmpty(depositaryRecord.getDepositPlace()), DepositaryRecord::getDepositPlace, depositaryRecord.getDepositPlace());
        lqw.like(Strings.isNotEmpty(depositaryRecord.getStudentNumber()), DepositaryRecord::getStudentNumber, depositaryRecord.getStudentNumber());
        lqw.like(true, DepositaryRecord::getApprovalStatus, depositaryRecord.getApprovalStatus());
        IPage page = new Page(currentPage, pageSize);
        depositaryRecordDao.selectPage(page, lqw);
        return page;
    }

}
