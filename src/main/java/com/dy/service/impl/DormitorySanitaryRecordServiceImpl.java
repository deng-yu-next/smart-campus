package com.dy.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dy.dao.DormitorySanitaryRecordDao;
import com.dy.dao.DormitorySanitaryRecordDao;
import com.dy.domain.DormitorySanitaryRecord;
import com.dy.domain.DormitorySanitaryRecord;
import com.dy.service.IDormitorySanitaryRecordService;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author dengyu
 * @since 2023-10-13
 */
@Service
public class DormitorySanitaryRecordServiceImpl extends ServiceImpl<DormitorySanitaryRecordDao, DormitorySanitaryRecord> implements IDormitorySanitaryRecordService {

    @Autowired
    private DormitorySanitaryRecordDao dormitorySanitaryRecordDao;

    @Override
    public IPage<DormitorySanitaryRecord> getPage(int currentPage, int pageSize) {
        IPage page = new Page(currentPage, pageSize);
        dormitorySanitaryRecordDao.selectPage(page, null);
        return page;
    }
    @Override
    public IPage<DormitorySanitaryRecord> getPage(int currentPage, int pageSize, DormitorySanitaryRecord dormitorySanitaryRecord) {
        LambdaQueryWrapper<DormitorySanitaryRecord> lqw = new LambdaQueryWrapper<DormitorySanitaryRecord>();
        lqw.like(Strings.isNotEmpty(dormitorySanitaryRecord.getDormitoryNumber()), DormitorySanitaryRecord::getDormitoryNumber, dormitorySanitaryRecord.getDormitoryNumber());
        lqw.like(true, DormitorySanitaryRecord::getInspectionResult, dormitorySanitaryRecord.getInspectionResult());
        lqw.like(true, DormitorySanitaryRecord::getClass, dormitorySanitaryRecord.getClass());
        IPage page = new Page(currentPage, pageSize);
        dormitorySanitaryRecordDao.selectPage(page, lqw);
        return page;
    }

}
