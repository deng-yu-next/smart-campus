package com.dy.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dy.dao.ReportingProcessDao;
import com.dy.domain.HolidayStaySchool;
import com.dy.domain.ReportingProcess;
import com.dy.service.IReportingProcessService;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author dengyu
 * @since 2023-10-13
 */
@Service
public class ReportingProcessServiceImpl extends ServiceImpl<ReportingProcessDao, ReportingProcess> implements IReportingProcessService {
    @Autowired
    private ReportingProcessDao reportingProcessDao;
    @Override
    public IPage<ReportingProcess> getPage(int currentPage, int pageSize) {
        IPage page = new Page(currentPage, pageSize);
        reportingProcessDao.selectPage(page, null);
        return page;
    }

    @Override
    public IPage<ReportingProcess> getPage(int currentPage, int pageSize, ReportingProcess reportingProcess) {
        LambdaQueryWrapper<ReportingProcess> lqw = new LambdaQueryWrapper<ReportingProcess>();

        lqw.like(Strings.isNotEmpty(reportingProcess.getCollegeName()), ReportingProcess::getCollegeName, reportingProcess.getCollegeName());
        lqw.like(Strings.isNotEmpty(reportingProcess.getTrainingInformation()), ReportingProcess::getTrainingInformation, reportingProcess.getTrainingInformation());
        lqw.like(Strings.isNotEmpty(reportingProcess.getSecurityInformation()), ReportingProcess::getSecurityInformation, reportingProcess.getSecurityInformation());
        lqw.like(Strings.isNotEmpty(reportingProcess.getCheckinInformation()), ReportingProcess::getCheckinInformation, reportingProcess.getCheckinInformation());
        IPage page = new Page(currentPage, pageSize);
        reportingProcessDao.selectPage(page, lqw);
        return page;
    }
}
