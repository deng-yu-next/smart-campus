package com.dy.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dy.dao.StudentDao;
import com.dy.domain.Student;
import com.dy.service.IStudentService;
import com.sun.org.apache.xpath.internal.operations.Bool;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author dengyu
 * @since 2023-10-13
 */
@Service
public class StudentServiceImpl extends ServiceImpl<StudentDao, Student> implements IStudentService {

    @Autowired
    private StudentDao studentDao;

    @Override
    public IPage<Student> getPage(int currentPage, int pageSize) {
        IPage page = new Page(currentPage, pageSize);
        studentDao.selectPage(page, null);
        return page;
    }

    @Override
    public IPage<Student> getPage(int currentPage, int pageSize, Student student) {
        LambdaQueryWrapper<Student> lqw = new LambdaQueryWrapper<Student>();

        lqw.like(Strings.isNotEmpty(student.getStudentNumber()), Student::getStudentNumber, student.getStudentNumber());
        lqw.like(Strings.isNotEmpty(student.getFullName()), Student::getFullName, student.getFullName());
        lqw.like(Strings.isNotEmpty(student.getNationalId()), Student::getNationalId, student.getNationalId());
        lqw.like(Strings.isNotEmpty(student.getCollegeName()), Student::getCollegeName, student.getCollegeName());
        lqw.like(Strings.isNotEmpty(student.getMajorName()), Student::getMajorName, student.getMajorName());
        IPage page = new Page(currentPage, pageSize);
        studentDao.selectPage(page, lqw);
        return page;
    }


}
