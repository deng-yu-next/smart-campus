package com.dy.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dy.dao.HolidayStaySchoolDao;
import com.dy.domain.HolidayStaySchool;
import com.dy.service.IHolidayStaySchoolService;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author dengyu
 * @since 2023-10-13
 */
@Service
public class HolidayStaySchoolServiceImpl extends ServiceImpl<HolidayStaySchoolDao, HolidayStaySchool> implements IHolidayStaySchoolService {
    @Autowired
    private HolidayStaySchoolDao holidayStaySchoolDao;

    @Override
    public IPage<HolidayStaySchool> getPage(int currentPage, int pageSize) {
        IPage page = new Page(currentPage, pageSize);
        holidayStaySchoolDao.selectPage(page, null);
        return page;
    }

    @Override
    public IPage<HolidayStaySchool> getPage(int currentPage, int pageSize, HolidayStaySchool holidayStaySchool){
        LambdaQueryWrapper<HolidayStaySchool> lqw = new LambdaQueryWrapper<HolidayStaySchool>();

        lqw.like(Strings.isNotEmpty(holidayStaySchool.getStudentNumber()), HolidayStaySchool::getStudentNumber, holidayStaySchool.getStudentNumber());
        lqw.like(true, HolidayStaySchool::getApprovalStatus, holidayStaySchool.getApprovalStatus());
        lqw.like(Strings.isNotEmpty(holidayStaySchool.getApplicationReason()), HolidayStaySchool::getApplicationReason, holidayStaySchool.getApplicationReason());
        lqw.like(true, HolidayStaySchool::getApplicationResult, holidayStaySchool.getApplicationResult());
        IPage page = new Page(currentPage, pageSize);
        holidayStaySchoolDao.selectPage(page, lqw);
        return page;
    }

}
