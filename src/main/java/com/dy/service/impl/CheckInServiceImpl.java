package com.dy.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dy.dao.CheckInDao;
import com.dy.domain.CampusCard;
import com.dy.domain.CheckIn;
import com.dy.service.ICheckInService;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author dengyu
 * @since 2023-10-13
 */
@Service
public class CheckInServiceImpl extends ServiceImpl<CheckInDao, CheckIn> implements ICheckInService {
    @Autowired
    private CheckInDao checkInDao;

    @Override
    public IPage<CheckIn> getPage(int currentPage, int pageSize) {
        IPage page = new Page(currentPage, pageSize);
        checkInDao.selectPage(page, null);
        return page;
    }
    @Override
    public IPage<CheckIn> getPage(int currentPage, int pageSize, CheckIn checkIn) {
        LambdaQueryWrapper<CheckIn> lqw = new LambdaQueryWrapper<CheckIn>();
        lqw.like(true, CheckIn::getCheckInStatus, checkIn.getCheckInStatus());
        lqw.like(Strings.isNotEmpty(checkIn.getStudentNumber()), CheckIn::getStudentNumber, checkIn.getStudentNumber());
        IPage page = new Page(currentPage, pageSize);
        checkInDao.selectPage(page, lqw);
        return page;
    }

}
