package com.dy.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dy.dao.DormitoryManagementDao;
import com.dy.dao.DormitoryManagementDao;
import com.dy.domain.DormitoryManagement;
import com.dy.domain.DormitoryManagement;
import com.dy.service.IDormitoryManagementService;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author dengyu
 * @since 2023-10-13
 */
@Service
public class DormitoryManagementServiceImpl extends ServiceImpl<DormitoryManagementDao, DormitoryManagement> implements IDormitoryManagementService {

    @Autowired
    private DormitoryManagementDao dormitoryManagementDao;

    @Override
    public IPage<DormitoryManagement> getPage(int currentPage, int pageSize) {
        IPage page = new Page(currentPage, pageSize);
        dormitoryManagementDao.selectPage(page, null);
        return page;
    }
    @Override
    public IPage<DormitoryManagement> getPage(int currentPage, int pageSize, DormitoryManagement dormitoryManagement) {
        LambdaQueryWrapper<DormitoryManagement> lqw = new LambdaQueryWrapper<DormitoryManagement>();
        lqw.like(Strings.isNotEmpty(dormitoryManagement.getDormitoryNumber()), DormitoryManagement::getDormitoryNumber, dormitoryManagement.getDormitoryNumber());
        lqw.like(Strings.isNotEmpty(dormitoryManagement.getStudent1Number()), DormitoryManagement::getStudent1Number, dormitoryManagement.getStudent1Number());
        lqw.like(Strings.isNotEmpty(dormitoryManagement.getStudent2Number()), DormitoryManagement::getStudent2Number, dormitoryManagement.getStudent2Number());
        lqw.like(Strings.isNotEmpty(dormitoryManagement.getStudent3Number()), DormitoryManagement::getStudent3Number, dormitoryManagement.getStudent3Number());
        lqw.like(Strings.isNotEmpty(dormitoryManagement.getStudent4Number()), DormitoryManagement::getStudent4Number, dormitoryManagement.getStudent4Number());
        IPage page = new Page(currentPage, pageSize);
        dormitoryManagementDao.selectPage(page, lqw);
        return page;
    }

}
