package com.dy.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dy.dao.FacultyDao;
import com.dy.dao.FacultyDao;
import com.dy.domain.Faculty;
import com.dy.domain.Faculty;
import com.dy.service.IFacultyService;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author dengyu
 * @since 2023-10-13
 */
@Service
public class FacultyServiceImpl extends ServiceImpl<FacultyDao, Faculty> implements IFacultyService {

    @Autowired
    private FacultyDao facultyDao;

    @Override
    public IPage<Faculty> getPage(int currentPage, int pageSize) {
        IPage page = new Page(currentPage, pageSize);
        facultyDao.selectPage(page, null);
        return page;
    }
    @Override
    public IPage<Faculty> getPage(int currentPage, int pageSize, Faculty faculty) {
        LambdaQueryWrapper<Faculty> lqw = new LambdaQueryWrapper<Faculty>();
        lqw.like(Strings.isNotEmpty(faculty.getCollegeName()), Faculty::getCollegeName, faculty.getCollegeName());
        lqw.like(true, Faculty::getGender, faculty.getGender());
        lqw.like(true, Faculty::getClass, faculty.getClass());
        IPage page = new Page(currentPage, pageSize);
        facultyDao.selectPage(page, lqw);
        return page;
    }
}
