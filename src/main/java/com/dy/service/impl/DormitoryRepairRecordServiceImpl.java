package com.dy.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dy.dao.DormitoryRepairRecordDao;
import com.dy.dao.DormitoryRepairRecordDao;
import com.dy.domain.DormitoryRepairRecord;
import com.dy.domain.DormitoryRepairRecord;
import com.dy.service.IDormitoryRepairRecordService;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author dengyu
 * @since 2023-10-13
 */
@Service
public class DormitoryRepairRecordServiceImpl extends ServiceImpl<DormitoryRepairRecordDao, DormitoryRepairRecord> implements IDormitoryRepairRecordService {

    @Autowired
    private DormitoryRepairRecordDao dormitoryRepairRecordDao;

    @Override
    public IPage<DormitoryRepairRecord> getPage(int currentPage, int pageSize) {
        IPage page = new Page(currentPage, pageSize);
        dormitoryRepairRecordDao.selectPage(page, null);
        return page;
    }
    @Override
    public IPage<DormitoryRepairRecord> getPage(int currentPage, int pageSize, DormitoryRepairRecord dormitoryRepairRecord) {
        LambdaQueryWrapper<DormitoryRepairRecord> lqw = new LambdaQueryWrapper<DormitoryRepairRecord>();
        lqw.like(Strings.isNotEmpty(dormitoryRepairRecord.getDormitoryNumber()), DormitoryRepairRecord::getDormitoryNumber, dormitoryRepairRecord.getDormitoryNumber());
        lqw.like(true, DormitoryRepairRecord::getRepairStatus, dormitoryRepairRecord.getRepairStatus());
        IPage page = new Page(currentPage, pageSize);
        dormitoryRepairRecordDao.selectPage(page, lqw);
        return page;
    }

}
