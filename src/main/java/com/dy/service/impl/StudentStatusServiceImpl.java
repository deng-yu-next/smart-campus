package com.dy.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dy.dao.StudentStatusDao;
import com.dy.domain.HolidayStaySchool;
import com.dy.domain.StudentStatus;
import com.dy.service.IStudentStatusService;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author dengyu
 * @since 2023-10-13
 */
@Service
public class StudentStatusServiceImpl extends ServiceImpl<StudentStatusDao, StudentStatus> implements IStudentStatusService {
    @Autowired
    private StudentStatusDao studentStatusDao;
    @Override
    public IPage<StudentStatus> getPage(int currentPage, int pageSize) {
        IPage page = new Page(currentPage, pageSize);
        studentStatusDao.selectPage(page, null);
        return page;
    }

    @Override
    public IPage<StudentStatus> getPage(int currentPage, int pageSize, StudentStatus studentStatus) {
        LambdaQueryWrapper<StudentStatus> lqw = new LambdaQueryWrapper<StudentStatus>();

        lqw.like(Strings.isNotEmpty(studentStatus.getStudentNumber()), StudentStatus::getStudentNumber, studentStatus.getStudentNumber());
        lqw.like(Strings.isNotEmpty(studentStatus.getApprovalStatus()), StudentStatus::getApprovalStatus, studentStatus.getApprovalStatus());
        lqw.like(Strings.isNotEmpty(studentStatus.getStartCollege()), StudentStatus::getStartCollege, studentStatus.getStartCollege());
        lqw.like(Strings.isNotEmpty(studentStatus.getUpdateCollege()), StudentStatus::getUpdateCollege, studentStatus.getUpdateCollege());
        lqw.like(Strings.isNotEmpty(studentStatus.getStartMajor()), StudentStatus::getStartMajor, studentStatus.getStartMajor());
        lqw.like(Strings.isNotEmpty(studentStatus.getUpdateMajor()), StudentStatus::getUpdateMajor, studentStatus.getUpdateMajor());
        lqw.like(Strings.isNotEmpty(studentStatus.getStartStudentType()), StudentStatus::getStartStudentType, studentStatus.getStartStudentType());
        lqw.like(Strings.isNotEmpty(studentStatus.getUpdateStudentType()), StudentStatus::getUpdateStudentType, studentStatus.getUpdateStudentType());
        lqw.like(Strings.isNotEmpty(studentStatus.getStartClass()), StudentStatus::getStartClass, studentStatus.getStartClass());
        lqw.like(Strings.isNotEmpty(studentStatus.getUpdateClass()), StudentStatus::getUpdateClass, studentStatus.getUpdateClass());
        lqw.like(Strings.isNotEmpty(studentStatus.getApplicationCause()), StudentStatus::getApplicationCause, studentStatus.getApplicationCause());
        lqw.like(true, StudentStatus::getApplicationType, studentStatus.getApplicationType());
        IPage page = new Page(currentPage, pageSize);
        studentStatusDao.selectPage(page, lqw);
        return page;
    }
}
