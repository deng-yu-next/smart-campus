package com.dy.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dy.dao.LeavingSchoolDao;
import com.dy.domain.HolidayStaySchool;
import com.dy.domain.LeavingSchool;
import com.dy.service.ILeavingSchoolService;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author dengyu
 * @since 2023-10-13
 */
@Service
public class LeavingSchoolServiceImpl extends ServiceImpl<LeavingSchoolDao, LeavingSchool> implements ILeavingSchoolService {
    @Autowired
    private LeavingSchoolDao leavingSchoolDao;
    @Override
    public IPage<LeavingSchool> getPage(int currentPage, int pageSize) {
        IPage page = new Page(currentPage, pageSize);
        leavingSchoolDao.selectPage(page, null);
        return page;
    }

    @Override
    public IPage<LeavingSchool> getPage(int currentPage, int pageSize, LeavingSchool leavingSchool) {
        LambdaQueryWrapper<LeavingSchool> lqw = new LambdaQueryWrapper<LeavingSchool>();

        lqw.like(Strings.isNotEmpty(leavingSchool.getStudentNumber()), LeavingSchool::getStudentNumber, leavingSchool.getStudentNumber());
        lqw.like(true, LeavingSchool::getApprovalStatus, leavingSchool.getApprovalStatus());
        lqw.like(true, LeavingSchool::getProcessingResult, leavingSchool.getProcessingResult());
        IPage page = new Page(currentPage, pageSize);
        leavingSchoolDao.selectPage(page, lqw);
        return page;
    }
}
