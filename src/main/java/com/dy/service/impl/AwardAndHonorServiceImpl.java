package com.dy.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dy.dao.AwardAndHonorDao;
import com.dy.domain.AwardAndHonor;
import com.dy.service.IAwardAndHonorService;
import com.sun.org.apache.xpath.internal.operations.Bool;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author dengyu
 * @since 2023-10-13
 */
@Service
public class AwardAndHonorServiceImpl extends ServiceImpl<AwardAndHonorDao, AwardAndHonor> implements IAwardAndHonorService {

    @Autowired
    private AwardAndHonorDao awardAndHonorDao;

    @Override
    public IPage<AwardAndHonor> getPage(int currentPage, int pageSize) {
        IPage page = new Page(currentPage, pageSize);
        awardAndHonorDao.selectPage(page, null);
        return page;
    }
    @Override
    public IPage<AwardAndHonor> getPage(int currentPage, int pageSize, AwardAndHonor awardAndHonor) {
        LambdaQueryWrapper<AwardAndHonor> lqw = new LambdaQueryWrapper<AwardAndHonor>();
        lqw.like(true, AwardAndHonor::getApprovalStatus, awardAndHonor.getApprovalStatus());
        lqw.like(Strings.isNotEmpty(awardAndHonor.getAwardName()), AwardAndHonor::getAwardName, awardAndHonor.getAwardName());
        lqw.like(Strings.isNotEmpty(awardAndHonor.getStudentNumber()), AwardAndHonor::getStudentNumber, awardAndHonor.getStudentNumber());
        lqw.like(true, AwardAndHonor::getAwardType, awardAndHonor.getAwardType());
        IPage page = new Page(currentPage, pageSize);
        awardAndHonorDao.selectPage(page, lqw);
        return page;
    }
}
