package com.dy.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dy.dao.DormitoryDisciplinaryRecordDao;
import com.dy.dao.DormitoryDisciplinaryRecordDao;
import com.dy.domain.DormitoryDisciplinaryRecord;
import com.dy.domain.DormitoryDisciplinaryRecord;
import com.dy.service.IDormitoryDisciplinaryRecordService;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author dengyu
 * @since 2023-10-13
 */
@Service
public class DormitoryDisciplinaryRecordServiceImpl extends ServiceImpl<DormitoryDisciplinaryRecordDao, DormitoryDisciplinaryRecord> implements IDormitoryDisciplinaryRecordService {

    @Autowired
    private DormitoryDisciplinaryRecordDao dormitoryDisciplinaryRecordDao;

    @Override
    public IPage<DormitoryDisciplinaryRecord> getPage(int currentPage, int pageSize) {
        IPage page = new Page(currentPage, pageSize);
        dormitoryDisciplinaryRecordDao.selectPage(page, null);
        return page;
    }
    @Override
    public IPage<DormitoryDisciplinaryRecord> getPage(int currentPage, int pageSize, DormitoryDisciplinaryRecord dormitoryDisciplinaryRecord) {
        LambdaQueryWrapper<DormitoryDisciplinaryRecord> lqw = new LambdaQueryWrapper<DormitoryDisciplinaryRecord>();
        lqw.like(Strings.isNotEmpty(dormitoryDisciplinaryRecord.getDormitory_number()), DormitoryDisciplinaryRecord::getDormitory_number, dormitoryDisciplinaryRecord.getDormitory_number());
        IPage page = new Page(currentPage, pageSize);
        dormitoryDisciplinaryRecordDao.selectPage(page, lqw);
        return page;
    }

}
