package com.dy.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dy.dao.WelcomeServiceDao;
import com.dy.domain.HolidayStaySchool;
import com.dy.domain.WelcomeService;
import com.dy.service.IWelcomeServiceService;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author dengyu
 * @since 2023-10-13
 */
@Service
public class WelcomeServiceServiceImpl extends ServiceImpl<WelcomeServiceDao, WelcomeService> implements IWelcomeServiceService {
    @Autowired
    private WelcomeServiceDao welcomeServiceDao;

    @Override
    public IPage<WelcomeService> getPage(int currentPage, int pageSize) {
        IPage page = new Page(currentPage, pageSize);
        welcomeServiceDao.selectPage(page, null);
        return page;
    }

    @Override
    public IPage<WelcomeService> getPage(int currentPage, int pageSize, WelcomeService welcomeService) {
        LambdaQueryWrapper<WelcomeService> lqw = new LambdaQueryWrapper<WelcomeService>();

        lqw.like(Strings.isNotEmpty(welcomeService.getCollegeName()), WelcomeService::getCollegeName, welcomeService.getCollegeName());
        lqw.like(Strings.isNotEmpty(welcomeService.getDepositaryLocation()), WelcomeService::getDepositaryLocation, welcomeService.getDepositaryLocation());
        lqw.like(Strings.isNotEmpty(welcomeService.getPickupLocation()), WelcomeService::getPickupLocation, welcomeService.getPickupLocation());
        IPage page = new Page(currentPage, pageSize);
        welcomeServiceDao.selectPage(page, lqw);
        return page;
    }
}
