package com.dy.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dy.dao.NetworkServiceDao;
import com.dy.domain.HolidayStaySchool;
import com.dy.domain.NetworkService;
import com.dy.service.INetworkServiceService;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author dengyu
 * @since 2023-10-13
 */
@Service
public class NetworkServiceServiceImpl extends ServiceImpl<NetworkServiceDao, NetworkService> implements INetworkServiceService {
    @Autowired
    private NetworkServiceDao networkServiceDao;
    @Override
    public IPage<NetworkService> getPage(int currentPage, int pageSize) {
        IPage page = new Page(currentPage, pageSize);
        networkServiceDao.selectPage(page, null);
        return page;
    }

    @Override
    public IPage<NetworkService> getPage(int currentPage, int pageSize, NetworkService networkService) {
        LambdaQueryWrapper<NetworkService> lqw = new LambdaQueryWrapper<NetworkService>();

        lqw.like(Strings.isNotEmpty(networkService.getStudentNumber()), NetworkService::getStudentNumber, networkService.getStudentNumber());
        lqw.like(true, NetworkService::getAccountBalance, networkService.getAccountBalance());
        lqw.like(true, NetworkService::getAccountStatus, networkService.getAccountStatus());
        IPage page = new Page(currentPage, pageSize);
        networkServiceDao.selectPage(page, lqw);
        return page;
    }
}
