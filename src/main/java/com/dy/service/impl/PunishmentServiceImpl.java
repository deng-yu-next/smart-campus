package com.dy.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dy.dao.PunishmentDao;
import com.dy.domain.HolidayStaySchool;
import com.dy.domain.Punishment;
import com.dy.service.IPunishmentService;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author dengyu
 * @since 2023-10-13
 */
@Service
public class PunishmentServiceImpl extends ServiceImpl<PunishmentDao, Punishment> implements IPunishmentService {
    @Autowired
    private PunishmentDao punishmentDao;
    @Override
    public IPage<Punishment> getPage(int currentPage, int pageSize) {
        IPage page = new Page(currentPage, pageSize);
        punishmentDao.selectPage(page, null);
        return page;
    }

    @Override
    public IPage<Punishment> getPage(int currentPage, int pageSize, Punishment punishment) {
        LambdaQueryWrapper<Punishment> lqw = new LambdaQueryWrapper<Punishment>();

        lqw.like(Strings.isNotEmpty(punishment.getStudentNumber()), Punishment::getStudentNumber, punishment.getStudentNumber());
        lqw.like(Strings.isNotEmpty(punishment.getApprovalStatus()), Punishment::getApprovalStatus, punishment.getApprovalStatus());
        lqw.like(Strings.isNotEmpty(punishment.getPunishmentInformation()), Punishment::getPunishmentInformation, punishment.getPunishmentInformation());
        lqw.like(true, Punishment::getPunishmentType, punishment.getPunishmentType());
        IPage page = new Page(currentPage, pageSize);
        punishmentDao.selectPage(page, lqw);
        return page;
    }
}
