package com.dy.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dy.dao.ReceptionActivitiesDao;
import com.dy.domain.HolidayStaySchool;
import com.dy.domain.ReceptionActivities;
import com.dy.service.IReceptionActivitiesService;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author dengyu
 * @since 2023-10-13
 */
@Service
public class ReceptionActivitiesServiceImpl extends ServiceImpl<ReceptionActivitiesDao, ReceptionActivities> implements IReceptionActivitiesService {
    @Autowired
    private ReceptionActivitiesDao receptionActivitiesDao;
    @Override
    public IPage<ReceptionActivities> getPage(int currentPage, int pageSize) {
        IPage page = new Page(currentPage, pageSize);
        receptionActivitiesDao.selectPage(page, null);
        return page;
    }

    @Override
    public IPage<ReceptionActivities> getPage(int currentPage, int pageSize, ReceptionActivities receptionActivities) {
        LambdaQueryWrapper<ReceptionActivities> lqw = new LambdaQueryWrapper<ReceptionActivities>();

        lqw.like(Strings.isNotEmpty(receptionActivities.getCollegeName()), ReceptionActivities::getCollegeName, receptionActivities.getCollegeName());
        lqw.like(Strings.isNotEmpty(receptionActivities.getActivityPlace()), ReceptionActivities::getActivityPlace, receptionActivities.getActivityPlace());
        lqw.like(Strings.isNotEmpty(receptionActivities.getActivityName()), ReceptionActivities::getActivityName, receptionActivities.getActivityName());
        lqw.like(Strings.isNotEmpty(receptionActivities.getActivityInformation()), ReceptionActivities::getActivityInformation, receptionActivities.getActivityInformation());
        IPage page = new Page(currentPage, pageSize);
        receptionActivitiesDao.selectPage(page, lqw);
        return page;
    }
}
