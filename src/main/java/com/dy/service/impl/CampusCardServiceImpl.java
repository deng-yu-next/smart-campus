package com.dy.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dy.dao.CampusCardDao;
import com.dy.domain.CampusCard;
import com.dy.service.ICampusCardService;
import com.sun.org.apache.xpath.internal.operations.Bool;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author dengyu
 * @since 2023-10-13
 */
@Service
public class CampusCardServiceImpl extends ServiceImpl<CampusCardDao, CampusCard> implements ICampusCardService {
    private CampusCardDao campusCardDao;

    @Override
    public IPage<CampusCard> getPage(int currentPage, int pageSize) {
        IPage page = new Page(currentPage, pageSize);
        campusCardDao.selectPage(page, null);
        return page;
    }
    @Override
    public IPage<CampusCard> getPage(int currentPage, int pageSize, CampusCard campusCard) {
        LambdaQueryWrapper<CampusCard> lqw = new LambdaQueryWrapper<CampusCard>();
        lqw.like(true, CampusCard::getAccountStatus, campusCard.getAccountStatus());
        lqw.like(Strings.isNotEmpty(campusCard.getStudentNumber()), CampusCard::getStudentNumber, campusCard.getStudentNumber());
        IPage page = new Page(currentPage, pageSize);
        campusCardDao.selectPage(page, lqw);
        return page;
    }
}
