package com.dy.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.dy.domain.HolidayStaySchool;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author dengyu
 * @since 2023-10-13
 */
public interface IHolidayStaySchoolService extends IService<HolidayStaySchool> {
    IPage<HolidayStaySchool> getPage(int currentPage, int pageSize);

    IPage<HolidayStaySchool> getPage(int currentPage, int pageSize, HolidayStaySchool holidayStaySchool);

}
