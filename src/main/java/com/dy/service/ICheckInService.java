package com.dy.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.dy.domain.CampusCard;
import com.dy.domain.CheckIn;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author dengyu
 * @since 2023-10-13
 */
public interface ICheckInService extends IService<CheckIn> {
    IPage<CheckIn> getPage(int currentPage, int pageSize);
    IPage<CheckIn> getPage(int currentPage, int pageSize, CheckIn checkIn);
}
