package com.dy;

import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.config.DataSourceConfig;

public class Generator {
    public static void main(String[] args) {
        AutoGenerator autoGenerator = new AutoGenerator();

        DataSourceConfig dataSource = new DataSourceConfig();
        dataSource.setDriverName("com.mysql.cj.jdbc.Driver");
        dataSource.setUrl("jdbc:mysql://localhost:3306/software_school?serverTimezone=UTC");
        dataSource.setUsername("root");
        dataSource.setPassword("2609079796aA@");
        autoGenerator.setDataSource(dataSource);

        autoGenerator.execute();
    }
}
