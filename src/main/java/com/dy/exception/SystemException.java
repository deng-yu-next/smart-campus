package com.dy.exception;

import com.dy.utils.Code;

//系统异常（SystemException）
public class SystemException extends RuntimeException {
    private Integer code;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public SystemException(Integer code, String message) {
        super(message);
        this.code = code;
    }

    public SystemException(Integer code, String message, Throwable cause) {
        super(message, cause);
        this.code = code;
    }

    public SystemException(Throwable cause) {
        super("服务器繁忙，请稍后重试!", cause);
        this.code = Code.SYSTEM_TIMEOUT_ERR;
    }

    public SystemException() {
        super("服务器繁忙，请稍后重试!");
        this.code = Code.SYSTEM_TIMEOUT_ERR;
    }
}
