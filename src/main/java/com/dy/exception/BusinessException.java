package com.dy.exception;

import com.dy.utils.Code;

//业务异常（BusinessException）
public class BusinessException extends RuntimeException{
    private Integer code;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public BusinessException(Integer code, String message) {
        super(message);
        this.code = code;
    }

    public BusinessException(Integer code, String message, Throwable cause) {
        super(message, cause);
        this.code = code;
    }

    public BusinessException(Throwable cause) {
        super("操作失误，请正确操作!",cause);
        this.code = Code.BUSINESS_ERR;
    }
    public BusinessException() {
        super("操作失误，请正确操作!");
        this.code = Code.BUSINESS_ERR;
    }
}
