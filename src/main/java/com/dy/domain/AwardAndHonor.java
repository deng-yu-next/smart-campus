package com.dy.domain;

import com.baomidou.mybatisplus.annotation.TableLogic;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * <p>
 *
 * </p>
 *
 * @author dengyu
 * @since 2023-10-13
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class AwardAndHonor implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    private String studentNumber;

    private Integer approvalStatus;

    private String applicationReason;

    private Integer awardType;

    private String awardName;

    @TableLogic
    private Integer deleted;


}
