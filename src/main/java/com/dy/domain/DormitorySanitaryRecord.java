package com.dy.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 *
 * </p>
 *
 * @author dengyu
 * @since 2023-10-13
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class DormitorySanitaryRecord implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    private String dormitoryNumber;

    private String inspectionResult;

    private LocalDateTime inspectionTime;

    @TableLogic
    private Integer deleted;


}
