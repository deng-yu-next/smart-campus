package com.dy.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.sql.Blob;

/**
 * <p>
 *
 * </p>
 *
 * @author dengyu
 * @since 2023-10-13
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class StudentStatus implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    private String studentNumber;

    private String approvalStatus;

    private String startCollege;

    private String updateCollege;

    private String startMajor;

    private String updateMajor;

    private String startStudentType;

    private String updateStudentType;

    private String startClass;

    private String updateClass;

    private String applicationCause;

    private Integer applicationType;

    private Blob submittedDocuments;

    @TableLogic
    private Integer deleted;


}
