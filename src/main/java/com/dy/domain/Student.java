package com.dy.domain;

import com.baomidou.mybatisplus.annotation.TableLogic;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * <p>
 *
 * </p>
 *
 * @author dengyu
 * @since 2023-10-13
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class Student implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    private String studentNumber;

    private String password;

    private String fullName;

    private String gender;

    private String nationalId;

    private String phoneNumber;

    private String email;

    private LocalDate startYear;

    private LocalDate finishYear;

    private String nationality;

    private LocalDate birthday;

    private String collegeName;

    private String majorName;

    private Integer studentType;

    private String className;

    private Integer counselorId;

    private String profileImageUrl;

    private Integer studentStatus;

    @TableLogic
    private Integer deleted;


}
