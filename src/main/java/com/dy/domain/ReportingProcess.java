package com.dy.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * <p>
 *
 * </p>
 *
 * @author dengyu
 * @since 2023-10-13
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class ReportingProcess implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    private String collegeName;

    private String trainingInformation;

    private String securityInformation;

    private String checkinInformation;

    @TableLogic
    private Integer deleted;


}
