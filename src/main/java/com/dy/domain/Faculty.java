package com.dy.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * <p>
 *
 * </p>
 *
 * @author dengyu
 * @since 2023-10-13
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class Faculty implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    private String employeeNumber;

    private String password;

    private String fullName;

    private String gender;

    private String nationalId;

    private String phoneNumber;

    private String email;

    private String nationality;

    private LocalDate birthday;

    private String collegeName;

    private Integer staffType;

    private String profileImageUrl;

    @TableLogic
    private Integer deleted;


}
